import pandas as pd
import savReaderWriter
import matplotlib.pyplot as plt
import networkx as nx
import Louvain

significance=2

file_name='FBS_analysis_07.06.2019.sav'

#scipy.io.readsav(file_name, idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)

with savReaderWriter.SavReader(file_name,ioUtf8=True) as reader:
    header = reader.header
#    for h in header:
#        print(h)
    df=pd.DataFrame(columns=header[1:-1])
    i=0
    for line in reader:
#        print(line)
#        print()
        df.loc[line[0]]=line[1:-1]
        i=i+1

#df.fillna(value=pd.np.nan, inplace=True)

#print(df.head())
df.to_csv('FBS_analysis.csv')

