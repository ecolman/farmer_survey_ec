import pandas as pd
import matplotlib.pyplot as plt
import networkx as nx
import Louvain


significance=2


dfx=pd.read_csv('better_key.csv',dtype=str,keep_default_na=False)

interpretation={}
for i,row in dfx.iterrows():
    interpretation[(row['Question'],row['Answer'])]=row['Meaning']

print(len(interpretation))

df=pd.read_csv('FBS_analysis.csv',dtype=str,index_col=0,keep_default_na=False)
#df.fillna(value=pd.np.nan, inplace=True)


#print(df['Q.9_Year_entered_farming'])


all_questions=list(df.columns.values)
removed=[]
questions=[]
print(len(all_questions))
for question in all_questions:
    if len(set(df[question].tolist()))==1:
        removed.append(question)
    else:
        questions.append(question)
print(len(questions))

##################### Questions ############################
## Make a list of questions we care about
#questions=[q for q in questions if q[2:4] in [str(i) for i in range(20,34)]]
#questions=questions+[q for q in all_questions if q[2:4]=='4a']
###########################################################


N=len(df)

cooccurrences={}
occurrences={}

in_count=0
out_count=0

qualities={}

for farmer,row in df.iterrows():
    #print(i)
    qualities[farmer]=[]

    for question in questions:
        # answer is the combination of question and answer 
        quality=(question,row[question])
        
#        if answer in interpretation:
#            
#            in_count=in_count+1
#        else:
#            out_count=out_count+1

        if quality in occurrences:
            occurrences[quality]=occurrences[quality]+1
        else:
            occurrences[quality]=1
        # each individual is like a layer with a complete network linking all their answers
        for other_quality in qualities[farmer]:
            
            combo=tuple(sorted([quality,other_quality]))

            if combo in cooccurrences:
                cooccurrences[combo]=cooccurrences[combo]+1
            else:
                cooccurrences[combo]=1
        qualities[farmer].append(quality)
        

#
#nodes=[]
#for combo in cooccurrences:
#    for node in combo:
##        print(node)
#        if isinstance(node[1], str)==False:
#            print(node[1])
#        
#        # remove answers that only occur once
#        if node in occurrences:
#            if occurrences[node]>1:                
#                nodes.append(node)
#nodes=list(set(nodes))            

# get the number of occurences of each answer (the degree of each node)



# calculate a z-score for each edge
# add to the network if and only of the score is above n sd's from the mean
edges=[]
z_dist=[0 for j in range(200)]

#G=nx.DiGraph()
G=nx.Graph()
Graph={}
for c in cooccurrences:
    
    # only add if answer occurred more than once
    if occurrences[c[0]]>10 and occurrences[c[1]]>10:
    
    
        z_pair=[]
        X=cooccurrences[c]
        # need to make it directed by switchig the order of the combo
        
        #print([(c[0],c[1]),(c[1],c[0])])
        for combo in [(c[0],c[1]),(c[1],c[0])]:
            #print(combo)
            ExpX=occurrences[combo[0]]*occurrences[combo[1]]/N
            VarX=(occurrences[combo[0]]*occurrences[combo[1]]/N)*(1-(occurrences[combo[1]]/N))
    
            z=(X-ExpX)/(VarX**(1/2))
            z_pair.append(z)
    
            n=100+int(10*z)
    
            z_dist[n]=z_dist[n]+1
            
        #print(z_pair)
        if z_pair[0]>significance and z_pair[1]>significance:
            edges.append([combo[0],combo[1]])
            G.add_edge(combo[0],combo[1])
            Graph[combo]=1
                
                    


plt.plot([((i-100)/10) for i in range(200)],z_dist)


print('Farmers:',N)
#print('N=',len(nodes))
print('E=',len(cooccurrences),sum(occurrences.values()))
print('W=',sum(cooccurrences.values()))

print('Edges:',len(edges))


####### Community detection stuff #################
# detect communties
colour_all=Louvain.get_colors(Graph)

# reduce
colour={}
size_of_community={}
for c in set(colour_all.values()):
    community=[quality for quality in colour_all if colour_all[quality]==c]
    size_of_community[c]=len(community)
    community_edges=[edge for edge in Graph if edge[0] in community and edge[1] in community]
    if len(community_edges)>1:
        for quality in community:
            colour[quality]=c
    


print('Communities:',len(set(colour.values())))

communities=list(set(colour.values()))


association_df=pd.DataFrame(columns=communities)


for farmer in qualities:
    associations=[0 for i in communities]
    for q in qualities[farmer]:
        if q in colour:
            index=communities.index(colour[q])
            
            associations[index]=associations[index]+1/size_of_community[colour[q]]
    
    
    association_df.loc[farmer]=associations
association_df.to_csv('association.csv')




##### Writing to file ######
output_file=open('communities.txt','w')

for c in set(colour.values()):
    community=[node for node in colour if colour[node]==c]

    ########## Part 2 ##########################
   
    output_file.write(str(c)+'\n')
    for quality in community:
        if quality in interpretation:
            #output_file.write(str(n[0])+'\t'+str(n[1])+'\t'+interpretation[n]+'\n')
            if interpretation[quality]!='':
                output_file.write(interpretation[quality]+' ('+str(occurrences[quality])+')\n')
#            else:
#                output_file.write(str(n[0])+'\t'+str(n[1])+'\t No interpretation yet \n')
    output_file.write('\n')

    #### Other cliques stuff that didn;t really work ####

    # create network x graph
#    G=nx.Graph()
#    for edge in community_edges:
#        G.add_edge(edge[0],edge[1])
#        
#    cliques=nx.find_cliques(G)
#    largest=0
#    for clique in cliques:
#        
#        if len(clique)>largest:
#            largest=len(clique)
#            largest_clique=clique
#
#    for node in largest_clique:
#        if node in interpretation:
#            if interpretation[node]!='':
#                print(interpretation[node])
#    print()

#output_file.close()
  

