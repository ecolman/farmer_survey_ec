Data files are stored in datastore data-restricted/FIELD_farmer_survey:
FBS_analysis_07.06.2019.sav
FBS_analysis.csv
better_key_(temp).csv
Copy of FBS Analysis Template BC 07.06.19.xlsx
better_key.csv

Codes in gitlab:
convert_SPSS_to_csv.py
make_better_key.py
Louvain.py
first_read.py
~~~~~~~~~~~~~~~~

File: 'FBS_analysis_07.06.2019.sav'

Provided by Beth Clark. Excel spreaddsheet with farmers as rows and questions as columns. Cells contain numbers to be interpreted by 'FBS Analysis Template BC 07.06.19'

Apply 'convert_SPSS_to_csv.py' to create 'FBS_analysis.csv'

File: 'FBS_analysis.csv'

Apply 'make_better_key.py' to 'FBS_analysis.csv' create 'better_key_(temp).csv'

File: 'Copy of FBS Analysis Template BC 07.06.19.xlsx'

Provided by Beth Clark via Maria Rodriguez. Provides interpretation of the numbers found in 'FBS_analysis_07.06.2019.sav'

Lots (i.e many hours work) of manual cross-referenceing with 'FBS Analysis Template BC 07.06.19' to add details (interpretations) to 'better_key_(temp).csv'. Save as  'better_key.csv'

File: 'better_key.csv'

Apply 'first_read.py' to create 'communities.txt' the output of the clustering procedure. Uses also 'Louvain.py'.






