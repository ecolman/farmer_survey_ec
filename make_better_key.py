import pandas as pd

df=pd.read_csv('FBS_analysis.csv',dtype=str, index_col=0,keep_default_na=False)
#df.fillna(value=pd.np.nan, inplace=True)

print(df['Q.9_Year_entered_farming'])

#print(df.head())


all_questions=list(df.columns.values)
removed=[]
questions=[]
#print(len(all_questions))
for question in all_questions:
    if len(set(df[question].tolist()))==1:
        removed.append(question)
    else:
        questions.append(question)
print(len(questions))


output_file=open('better_key_(temp).csv','w')
output_file.write('Question,Answer\n')

for question in questions:
        
    answers=list(set(df[question].tolist()))
    for answer in answers:
        
        output_file.write(question+','+str(answer)+'\n')
        
    if question=='Q.9_Year_entered_farming':
        print(question,answers)
        
output_file.close()
